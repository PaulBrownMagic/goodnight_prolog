
frame(personality, [ val-openess
                   , val-concientiousness
                   , val-extraversion
                   , val-agreeable
                   , val-neuroticism
                   ]
               ).

frame(needs, [ val-physiological:[air, water, shelter, sleep, clothing, reproduction]
             , val-safety:[security, employment, resources, health, property]
             , val-love:[friendship,intimacy,family,connection]
             , val-esteem:[respect, status, recognition, strength, freedom]
             , val-self_actualisation:[accomplishment]
         ]).

frame(emotions, [val-[ecstacy, joy, serenity]
                ,val-[admiration, trust, acceptance]
                ,val-[terror, fear, apprehension]
                ,val-[amazement, surprise, distraction]
                ,val-[grief, sadness, pensiveness]
                ,val-[loathing, disgust, boredom]
                ,val-[rage, anger, annoyance]
                ,val-[vigilance, anticipation, interest]
            ]
        ).

frame(relationships, [val-[partner, friend, aquaintence, dislike, enemy]
                     ,val-[dominant, equal, submissive]
                     ]
                 ).
