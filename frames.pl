:- module(frames, [get_frame/2
                         , add_frame/2
                         , del_frame/2
                         , components/1
                         ]
         ).

/** <module> frames
frames Module adapts the Frames implementation in "Building
Expert frames in Prolog" by Dennis Merritt to use for frames and
ports the code to SWI-Prolog.

Porting required removing member/2 to use the built-in for a performance
increase. It also required the definition of list/1, which checks if its
argument is a non-empty list, whereas SWI-Prolog's is_list/1 only checks
if it's argument is a list. Finally the module structure has been implemented,
and pldoc comments added for documentation generation (doc_server(9000)).

Adapting for frames was done by renaming 'frame' to 'frame' to aid
comprehension, and the addition of components/1. This new predicate
demonstrates a transitive search of frame components from a given root.
Further predicates can be added to explore the frame structure and its
fluents.

@license original: MIT License
@copyright original: Amzi! inc. 2016
@author original: Dennis Merritt, adaptations: Paul Brown

*/

%! facet_list(-Facets) is semidet.
%  A list of the facets that determine the behaviour of the fluent.
%
%  @arg Facets [val, def, calc, add, del, edit] meaning
%  value, default, calculate, add, delete, edit.
%
facet_list([val,def,calc,add,del,edit]).


%! prep_req(+SlotValue, -Req) is nondet.
%
% @arg SlotValue  the name of the fluent(Slot) and it's value (Value)
% separated by a hyphen: slot-value. Value may be a variable.
%
% @arg Req is req(Name, Slot, Facet, Value)  A more accurate and useful format
% of Slot-Value.
%
prep_req(Slot-X,req(_,Slot,val,X)) :- var(X), !.
prep_req(Slot-X,req(_,Slot,Facet,Val)) :-
	nonvar(X),
	X =.. [Facet,Val],
	facet_list(FL),
	member(Facet,FL), !.
prep_req(Slot-X,req(_,Slot,val,X)).


%! get_frame(Name, ReqList) is nondet.
%  get_frame is primarily used to retrieve a list of slot values,
%  it can also be used to find frames that match a required list.
%
%  @arg Name a frame name.
%  @arg ReqList when provided, the required fluents.
%  When a variable, all the fluents for the given Name.
get_frame(Name, ReqList) :-
	frame(Name, SlotList),
	slot_vals(Name, ReqList, SlotList).

%! slot_vals(Thing, ReqList, SlotList) is nondet.
%  Recursion over required slot list to find slots via [[find_slot/2]]
slot_vals(_,[],_).
slot_vals(T,[Req|Rest],SlotList) :-
	prep_req(Req,req(T,S,F,V)),
	find_slot(req(T,S,F,V),SlotList),
	!, slot_vals(T,Rest,SlotList).
slot_vals(T, Req, SlotList) :-
	prep_req(Req,req(T,S,F,V)),
	find_slot(req(T,S,F,V), SlotList).

%! find_slot(Req, SlotList) is nondet.
%  Find a slot that unifies with the requirement.
%  Supports inheritance through ako- relationship.
find_slot(req(T,S,F,V), SlotList) :-
	nonvar(V), !,
	find_slot(req(T,S,F,Val), SlotList), !,
	(Val == V; member(V,Val)).
find_slot(req(T,S,F,V), SlotList) :-
	member(S-FacetList, SlotList), !,
	facet_val(req(T,S,F,V),FacetList).
find_slot(req(T,S,F,V), SlotList) :-
	member(ako-FacetList, SlotList),
	facet_val(req(T,ako,val,Ako),FacetList),
	(member(X,Ako); X = Ako),
	frame(X, HigherSlots),
	find_slot(req(T,S,F,V), HigherSlots), !.
find_slot(Req,_) :-
	error(['frame error looking for:',Req]).

%! facet_val(Req, FacetList) is nondet.
%  Get's the value of the required facet.
facet_val(req(_,_,F,V),FacetList) :-
	FV =.. [F,V],
	member(FV,FacetList), !.
facet_val(req(_,_,val,V),FacetList) :-
	member(val ValList,FacetList),
	member(V,ValList), !.
facet_val(req(_,_,val,V),FacetList) :-
	member(def V,FacetList), !.
facet_val(req(T,S,val,V),FacetList) :-
	member(calc Pred,FacetList),
	CalcPred =.. [Pred,req(T,S,val,V)],
	call(CalcPred).

%! add_frame(+Name, +UList:list) is semidet.
% adds a list of slot values, so can create a new frame or change one.
%
% @arg Name a frame name
% @arg UList a list of fluents to add.
add_frame(Name, UList) :-
	old_slots(Name,SlotList),
	add_slots(Name,UList,SlotList,NewList),
	retract(frame(Name,_)),
	asserta(frame(Name,NewList)), !.

%! old_slots(Name, SlotList) is nondet.
%  Usually retrieves the slot list, however if the frame
%  doesn't exist it creates a new frame with an empty
%  slot list.
old_slots(Name,SlotList) :-
	frame(Name,SlotList), !.
old_slots(Name,[]) :-
	asserta(frame(Name,[])).

%! add_slots(Thing, Req, Slots, NewList) is nondet.
%  Deletes the old slot and associated facet from the old
%  slot list. Then adds the new facet plus value to make
%  a new list.
add_slots(_,[],X,X).
add_slots(T,[U|Rest],SlotList,NewList) :-
	prep_req(U,req(T,S,F,V)),
	add_slot(req(T,S,F,V),SlotList,Z),
	add_slots(T,Rest,Z,NewList).
add_slots(T,X,SlotList,NewList) :-
	prep_req(X,req(T,S,F,V)),
	add_slot(req(T,S,F,V),SlotList,NewList).

%! add_slot(Req, SlotList) is nondet.
%  adds the new slot (facet and value) to the slot list.
add_slot(req(T,S,F,V),SlotList,[S-FL2|SL2]) :-
	delete(S-FacetList,SlotList,SL2),
	add_facet(req(T,S,F,V),FacetList,FL2).

%! add_facet(Req, FacetList, NewList) is nondet.
%  deletes the old facet from the list, builds a
%  new facet and adds it to the facet list. Supports
%  terms and lists via [[add_newval/3]].
add_facet(req(T,S,F,V),FacetList,[FNew|FL2]) :-
	FX =.. [F,OldVal],
	delete(FX,FacetList,FL2),
	add_newval(OldVal,V,NewVal),
	!, check_add_demons(req(T,S,F,V),FacetList),
	FNew =.. [F,NewVal].

%! add_newval(X, Val, Val) is nondet.
%  aids [[add_facet/3]] to handle lists and terms.
add_newval(X,Val,Val) :- var(X), !.
add_newval(OldList,ValList,NewList) :-
	list(OldList),
	list(ValList),
	append(ValList,OldList,NewList), !.
add_newval([H|T],Val,[Val,H|T]).
add_newval(_,Val,Val).

%! check_add_demons(Req, _) is nondet.
%  checks to see if any daemon predicates (facet = add)
%  need to be called and calls them.
check_add_demons(req(T,S,F,V),_) :-
	get_frame(T,S-add(Add)), !,
	AddFunc =.. [Add,req(T,S,F,V)],
	call(AddFunc).
check_add_demons(_,_).


%! del_frame(+Name) is semidet.
% delete a whole frame.
del_frame(Name) :-
	retract(frame(Name,_)).
del_frame(Name) :-
	error(['No frame',Name,'to delete']).

%! del_frame(+Name, +UList:list) is semidet.
%  delete fluents from a frame
%
%  @arg Name is a frame name
%  @arg UList is the list of fluents to delete.
del_frame(Name, UList) :-
	old_slots(Name,SlotList),
	del_slots(Name,UList,SlotList,NewList),
	retract(frame(Name,_)),
	asserta(frame(Name,NewList)).

%! del_slots(Thing, Req, Slots, NewList) is nondet.
% Deletes the required slot and associated facet from the slot list.
del_slots([], X, X, _).
del_slots(T,[U|Rest],SlotList,NewList) :-
	prep_req(U,req(T,S,F,V)),
	del_slot(req(T,S,F,V),SlotList,Z),
	del_slots(T,Rest,Z,NewList).
del_slots(T,X,SlotList,NewList) :-
	prep_req(X,req(T,S,F,V)),
	del_slot(req(T,S,F,V),SlotList,NewList).

%! del_slot(Req, SlotList) is nondet.
% removes the required slot from the slot list.
del_slot(req(T,S,F,V),SlotList,[S-FL2|SL2]) :-
	remove(S-FacetList,SlotList,SL2),
	del_facet(req(T,S,F,V),FacetList,FL2).
del_slot(Req,_,_) :-
	error(['del_slot - unable to remove',Req]).

%! del_facet(Req, FacetList, NewList) is nondet.
% removes the face from the list. Supports terms and lists.
% Note, cannot delete a default facet or an inherited facet.
del_facet(req(T,S,F,V),FacetList,FL) :-
	FV =.. [F,V],
	remove(FV,FacetList,FL),
	!, check_del_demons(req(T,S,F,V),FacetList).
del_facet(req(T,S,F,V),FacetList,[FNew|FL]) :-
	FX =.. [F,OldVal],
	remove(FX,FacetList,FL),
	remove(V,OldVal,NewValList),
	FNew =.. [F,NewValList],
	!, check_del_demons(req(T,S,F,V),FacetList).
del_facet(Req,_,_) :-
	error(['del_facet - unable to remove',Req]).

%! check_del_demons(Req, _) is nondet.
% checks to see if any daemon predicates (facet = del)
% need to be called and calls them.
check_del_demons(req(T,S,F,V),_) :-
	get_frame(T,S-del(Del)), !,
	DelFunc =.. [Del,req(T,S,F,V)],
	call(DelFunc).
check_del_demons(_,_).

%! print_frames is nondet.
% print frames via [[print_frame/1]].
print_frames :-
	frame(Name, _),
	print_frame(Name),
	fail.
print_frames.

%! print_frame(Name) is nondet.
%  prints the named frame, slots printed via [[print_slots/1]].
print_frame(Name) :-
	frame(Name, SlotList),
	write_line(['frame:',Name]),
	print_slots(SlotList), nl.

%! print_slots(Slots) is det.
%  print out the given slots.
print_slots([]).
print_slots([Slot|Rest]) :-
	write_line(['  Slot:',Slot]),
	print_slots(Rest).

% utilities

%! list(L) is semidet.
%  Checks if the given argument is a non-empty list.
list(L) :- is_list(L), L \= [].

%! delete(X, L1, L2) is nondet.
%  delete/3 removes the given item (X) from a list (L1). If the item
%  was not found, the orginal list is returned.
delete(_,[],[]).
delete(X,[X|Y],Y) :- !.
delete(X,[Y|Z],[Y|W]) :- delete(X,Z,W).

%! remove(X, L1, L2) is nondet.
%  remove/3 removes a given item (X) from a list (L1). Similar
%  to [[delete/3]], except if the item is not found in the list,
%  remove/3 will fail.
remove(X,[X|Y],Y) :- !.
remove(X,[Y|Z],[Y|W]) :- remove(X,Z,W).

errors(off).  %on/off

error(_) :- errors(off), !, fail.
error(E) :-
	nl, write('*** '),
	write_line(E),
	write(' ***'), nl,
	fail.

write_line([]) :- nl.
write_line([H|T]) :-
	write(H),tab(1),
	write_line(T).
